function simplePoller(queryFn,callback) {
    //  定义初始化时间（1秒）
    let time = 1000;
    //  声明定期执行runQueryFn函数方法，用来递归执行。
    function runQueryFn(){
        return setTimeout(() => {
            //  如果返回true则执行callback回调
            if(queryFn()){
                callback();
                //  手动释放内存
                return time = runQueryFn = null;
            }else{
                //  在原来时间等待间隔的基础上加上1.5倍
                time*=1.5;
                //  递归执行 runQueryFn 直到返回true 为止
                runQueryFn();
                console.log('0.0 queryFn返回 false 继续递归');
            }
        }, time);
    }
    return runQueryFn();
}

// simplePoller(()=>{
//     let result = Math.random()*10;
//     console.log(result);
//     return result<6.4?false:true;
// },()=>{
//     console.log("终于成功了 test 1")
// });

// simplePoller(()=>{
//     let result = Math.random()*10;
//     console.log(result);
//     return result<6.4?false:true;
// },()=>{
//     console.log("终于成功了 test 2")
// });

// simplePoller(()=>{
//     let result = Math.random()*10;
//     console.log(result);
//     return result<6.4?false:true;
// },()=>{
//     console.log("终于成功了  test 3")
// });