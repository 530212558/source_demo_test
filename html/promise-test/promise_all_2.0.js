// 三种状态
const PENDING = "pending";
const RESOLVED = "resolved";
const REJECTED = "rejected"

function MyPromise(fn) {
  let _this = this;
  this.promiseState = PENDING;
  this.promiseResult = undefined;

  const resolve = function (promiseResult) {
    queueMicrotask(() => { // 异步执行，保证执行顺序
      _this.promiseResult = promiseResult;
      _this.promiseState = RESOLVED;
    })
  };

  const reject = function (reason) {
    queueMicrotask(() => { // 异步执行，保证执行顺序
      _this.promiseResult = reason;
      _this.promiseState = REJECTED;
    })
  }

  try {
    fn(resolve, reject);
  } catch(e){
    reject(e)
  }
}

MyPromise.prototype.then = function(onResolved, onRejected) {
  let promise;
  const onResolvedIsFun = typeof onResolved === 'function' ? true : false;
  const onRejectedIsFun = typeof onRejected === 'function' ? true : false;

  let promiseState = this.promiseState;
  Object.defineProperty(this, "promiseState", {
    get() {
      return promiseState;
    },
    set(newValue) {
      if(promiseState!== PENDING) return;
      let result;
      if(onResolvedIsFun&&newValue === RESOLVED) {
        result = onResolved(this.promiseResult);
      } else if(onRejectedIsFun&&newValue === REJECTED) {
        result = onRejected(this.promiseResult);
      }
      promiseState = newValue;

      if(!onRejectedIsFun&&newValue === REJECTED){  //传递失败状态
        promise.promiseResult = this.promiseResult;
        promise.promiseState = REJECTED;
      }else if( result instanceof MyPromise ){  //返回新Promise再次监听promiseState状态
        let value = result.promiseState;
        Object.defineProperty(result, "promiseState", {
          get() {
            return value;
          },
          set(newValue) {
            if(value!== PENDING) return;
            value = newValue;
            promise.promiseResult = result.promiseResult;
            promise.promiseState = newValue;
          }
        });
      }else{  //默认返回成功
        promise.promiseState = RESOLVED;
      }
    }
  });
  return (promise = new MyPromise(function(){}));
}

MyPromise.prototype.catch = function (onRejected) {
  let promise;
  const onRejectedIsFun = typeof onRejected === 'function' ? true : false;
  let promiseState = this.promiseState;
  Object.defineProperty(this, "promiseState", {
    get() {
      return promiseState;
    },
    set(newValue) {
      if(promiseState!== PENDING) return;
      let result;
      if(onRejectedIsFun&&newValue === REJECTED) {
        result = onRejected(this.promiseResult);
      }
      promiseState = newValue;
      if(!onRejectedIsFun&&newValue === REJECTED){  //传递失败状态
        promise.promiseResult = this.promiseResult;
        promise.promiseState = REJECTED;
      }else if( result instanceof MyPromise ){  //返回新Promise再次监听promiseState状态
        let value = result.promiseState;
        Object.defineProperty(result, "promiseState", {
          get() {
            return value;
          },
          set(newValue) {
            if(value!== PENDING) return;
            value = newValue;
            promise.promiseResult = result.promiseResult;
            promise.promiseState = newValue;
          }
        });
      }else{  //默认返回成功
        promise.promiseState = RESOLVED;
      }
    }
  });
  return (promise = new MyPromise(function(){}));
}

MyPromise.all = function (promises) {
  promises = Array.from(promises);
  let promise;
  if(promises.length<=0){
    promise = new MyPromise(function(resolve){
      resolve();
    });
    return promise;
  }
  const result = {
    length: 0
  }
  for (let [index,value] of promises.entries()) {
    let promiseState = value.promiseState;
    Object.defineProperty(value, "promiseState", {
      get() {
        return promiseState;
      },
      set(newValue) {
        if(promise.promiseState!==PENDING) return;
        promiseState = newValue;
        if(newValue===REJECTED){
          promise.promiseResult = value.promiseResult;
          promise.promiseState = REJECTED;
          return;
        }
        result[index] = value.promiseResult;
        result.length++;
        if(result.length===promises.length){
          promise.promiseResult = Array.from(result);
          promise.promiseState = RESOLVED;
        }
      }
    });
  }
  return (promise = new MyPromise(function(){}));
}




