
const net=require('net');

// console.log(123);

function portIsOccupied (port){
    const server=net.createServer().listen(port)
    return new Promise((resolve,reject)=>{
        server.on('listening',()=>{
            console.log(`the server is runnint on port ${port}`)
            server.close()
            resolve(port)
        })

        server.on('error',(err)=>{
            if(err.code==='EADDRINUSE'){
                resolve(portIsOccupied(port+1))//注意这句，如占用端口号+1
                console.log(`this port ${port} is occupied.try another.`)
            }else{
                reject(err)
            }
        })
    })

}
async function startApp(){
    try{
        const port=await portIsOccupied(3000)
        // app.listen(port,()=>{
        //     console.log(`start http://localhost:${port}`)
        //     c.exec(`start http://localhost:${port}`)
        // })
        console.log(port,'===>port')
        return port;
    }catch(err){
        console.error(err)
    }
    return 123
}

console.log(startApp());
console.log("startApp() start");

// portIsOccupied(3000,(err,port)=>{
//     // app.listen(port,()=>{
//     //     console.log(`start http://localhost:${port}`)
//     //     c.exec(`start http://localhost:${port}`)
//     // })
// })

module.exports=portIsOccupied
