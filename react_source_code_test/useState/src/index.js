// import React from 'react';
// import ReactDOM from 'react-dom/client';
// import './index.css';
// import App from './App';
// import reportWebVitals from './reportWebVitals';

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>
// );

// // If you want to start measuring performance in your app, pass a function
// // to log results (for example: reportWebVitals(console.log))
// // or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();

import React from "react";
import ReactDOM from "react-dom/client";

let workInProgressHook;//当前工作中的hook
let isMount = true;//是否时mount时

const fiber = {//fiber节点
  memoizedState: null,//hook链表
  stateNode: App//dom
};


function mountWorkInProgressHook() {//mount时调用
  const hook = {//构建hook
    queue: {//更新队列
      pending: null//未执行的update队列
    },
    memoizedState: null,//当前state
    next: null//下一个hook
  };
  if (!fiber.memoizedState) {
    fiber.memoizedState = hook;//第一个hook的话直接赋值给fiber.memoizedState
  } else {
    workInProgressHook.next = hook;//不是第一个的话就加在上一个hook的后面，形成链表
  }
  workInProgressHook = hook;//记录当前工作的hook
  return workInProgressHook;
}
function updateWorkInProgressHook() {//update时调用
  let curHook = workInProgressHook;
  workInProgressHook = workInProgressHook.next;//下一个hook
  return curHook;
}
function useState(initialState) {
  let hook;
  if (isMount) {
    hook = mountWorkInProgressHook();
    hook.memoizedState = initialState;//初始状态
  } else {
    hook = updateWorkInProgressHook();
  }

  let baseState = hook.memoizedState;//初始状态
  if (hook.queue.pending) {
    let firstUpdate = hook.queue.pending;//第一个update
    baseState = firstUpdate.action(baseState);

      // do {
      //   const action = firstUpdate.action;
      //   baseState = action(baseState);
      //   firstUpdate = firstUpdate.next;//循环update链表
      // } while (firstUpdate !== hook.queue.pending);//通过update的action计算state

    hook.queue.pending = null;//重置update链表
  }
  hook.memoizedState = baseState;//赋值新的state

  return [baseState, dispatchAction.bind(null, hook.queue)];//useState的返回
}

function dispatchAction(queue, action) {//触发更新
  const update = {//构建update
    action,
    next: null
  };
  // if (queue.pending === null) {
  //   update.next = update;//update的环状链表
  // } else {
  //   update.next = queue.pending.next;//新的update的next指向前一个update
  //   // console.log(update,queue.pending.next);
  //   queue.pending.next = update;//前一个update的next指向新的update
  // }
  queue.pending = update;//更新queue.pending
  // queue.pending.next = update;

  isMount = false;//标志mount结束
  // console.log(fiber);
  workInProgressHook = fiber.memoizedState;//更新workInProgressHook
  // console.log(workInProgressHook,action);
  schedule();//调度更新
}

function App() {
  let [count, setCount] = useState(1);
  // let [age, setAge] = useState(10);
  return (
    <>
      <p>Clicked {count} times</p>
      <button onClick={() => {
        setCount(() => count + 1)
        setCount(() => count + 1)
        setCount(() => count + 1)
        setCount(() => count + 1)
        setCount(() => count + 1)
        setCount(() => count + 1)
      }}> Add count</button>
      {/* <p>Age is {age}</p> */}
      {/* <button onClick={() => setAge(() => age + 1)}> Add age</button> */}
    </>
  );
}

const root = ReactDOM.createRoot(document.getElementById('root'));

// function App() {
//   return _react.default.createElement(
//     "div", 
//     {
//       class: "add"
//     }, 
//     _react.default.createElement("h1", null, "header"), "Hello World", _react.default.createElement("h1", null, "footer")
//   );
// }
function schedule() {
  root.render(
    <React.StrictMode>
      <App />
    </React.StrictMode>
  );
}


schedule();



// 请问：

// 1.箭头函数中的this指向，和普通函数中的this指向有何区别
// ()=>{
// this~~~?
// }

// function func(){
// this~~~?
// }
// （1）箭头函数内的this对象，就是定义时所在的对象，而不是使用时所在的对象。

// （2）箭头函数不可以当作构造函数，也就是说，不可以使用new命令，否则会抛出一个错误。

// （3）箭头函数不可以使用arguments对象，该对象在函数体内不存在。如果要用，可以用Rest参数代替。

// （4）箭头函数不可以使用yield命令，因此箭头函数不能用作Generator函数。





// 2.请手写一个generator函数自动执行器，执行以下函数(以下语言为ts)

// export function runGenAuto(fn: Function) {
//     // 需要编写的内容
// }

// interface Param {
//     [key: string]: any
// }
// export function* transferToObj(params: URLSearchParams) {
//     let result: Param = {};
//     for (let [key, value] of params.entries()) {
//         yield (next: Function) => next(result[key] = value)
//     }
//     return result
// }

// let params = runGenAuto(() => transferToObj(new URLSearchParams("q=apple&from=en&to=zh&appid=2015063000000001&salt=1435660288&sign=f89f9594663708c1605f3d736d01d2d4")))
// console.log(params)